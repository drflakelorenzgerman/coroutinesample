package com.sass.coroutinessample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main

class MainActivity : AppCompatActivity() {

    val maxCount = 10
    var counter = 0

    lateinit var job : Job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        CoroutineScope(IO).launch {
            doTask1()
        }
        CoroutineScope(Main).launch {
            // do some UI
        }

        btn.setOnClickListener {
            if(!::job.isInitialized)
                createJob()
        }

        btn_cancel.setOnClickListener {
            if(::job.isInitialized)
                job.cancel()
        }
    }

    private fun counter(job : Job)
    {
        if(counter < maxCount)
        {
            CoroutineScope(IO + job).launch {
                counter++
                toast(counter.toString())
                delay(1000)
                createJob()

            }
        }
    }

    private fun createJob()
    {
        job = Job()
        job.invokeOnCompletion {
            it?.message.let {
                if(it != null)
                    toast(it)
            }
        }
        counter(job)
    }

    fun toast(str : String)
    {
        GlobalScope.launch(Main) {
            Toast.makeText(this@MainActivity,
                str,Toast.LENGTH_SHORT).show()
        }
    }


    private suspend fun doTask1()
    {
        delay(1000)
        Log.d("TASK1","My Task");
        withContext(Main){
            // do some UI
        }
    }

//    private suspend fun doTask2()
//    {
//        delay(1000)
//        Log.d("TASK2","My Task");
//        withContext(Main){
//            txt.text = "DONE"
//        }
//    }
}
